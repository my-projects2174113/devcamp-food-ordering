$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // Khai báo hằng số API
  const gBASE_URL = "https://food-ordering-fvo9.onrender.com/api";
  // Biến toàn cục để lưu và xử lý hiển thị STT
  var gSTT = 1;
  // Biến toàn cục để lưu ID dành cho update và delete
  var gId = "";
  // Biến toàn cục để lưu voucher ID dành cho update và delete
  var gVoucherId = "";
  // Khai báo biến toàn cục để lưu mảng vouchers(lát đổ form)
  var gVoucherListArr = [];
  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gORDER_COLS = [
    "id",
    "orderCode",
    "fullname",
    "methodPayment",
    "foods",
    "price",
    "createdAt",
    "action",
  ];
  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gORDER_ID_COL = 0;
  const gORDER_CODE_COL = 1;
  const gORDER_NAME_COL = 2;
  const gORDER_METHOD_PAYMENT_COL = 3;
  const gORDER_FOODS_COL = 4;
  const gORDER_PRICE_COL = 5;
  const gORDER_CREATE_DAY_COL = 6;
  const gORDER_ACTION_COL = 7;
  // Biến html table thành DataTable và Khởi tạo DataTable, đồng thời gán biến để dễ dùng sau này
  var gTable = $("#orders-table").DataTable({
    // searching: false,
    // ordering: false,
    columns: [
      { data: gORDER_COLS[gORDER_ID_COL] },
      { data: gORDER_COLS[gORDER_CODE_COL] },
      { data: gORDER_COLS[gORDER_NAME_COL] },
      { data: gORDER_COLS[gORDER_METHOD_PAYMENT_COL] },
      { data: gORDER_COLS[gORDER_FOODS_COL] },
      { data: gORDER_COLS[gORDER_PRICE_COL] },
      { data: gORDER_COLS[gORDER_CREATE_DAY_COL] },
      { data: gORDER_COLS[gORDER_ACTION_COL] },
    ],
    columnDefs: [
      {
        targets: gORDER_ID_COL,
        render: function () {
          return gSTT++; //có thể dùng meta lấy index row +1
        },
      },
      {
        targets: gORDER_NAME_COL,
        render: function (paramData, paramType, paramRow, paramMeta) {
          return paramRow.lastName + paramRow.firstName;
        },
      },
      {
        targets: gORDER_FOODS_COL,
        render: function (paramData) {
          return paramData.length;
        },
      },
      {
        targets: gORDER_PRICE_COL,
        render: function (paramData, paramType, paramRow, paramMeta) {
          let vPrice = 0;
          //Lấy mảng foods rồi lặp bằng forEach(dùng while thì hợp lý hơn)
          // Với mỗi phần tử food sẽ cộng thêm giá tiền vào giá tổng
          // Khai báo vPrice bên ngoài vì forEach chỉ thực hiện action chứ ko trả về giá trị
          paramRow.foods.forEach((paramFood) => {
            vPrice += paramFood.price;
          });
          return "$" + vPrice;
        },
      },
      {
        targets: gORDER_ACTION_COL,
        defaultContent: `
        <div class="d-flex justify-content-around">
        <i class="fa-solid fa-pen-to-square update-icon btn mx-auto"></i>
        <i class="fa-solid fa-trash-can delete-icon btn mx-auto"></i>
        </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading(); //gọi hàm load trang
  // gọi hàm xử lý call API get vouchers list
  callAPIGetVouchersList();

  // Lắng nghe sự kiện DataTable được vẽ xong(draw)
  gTable.on("draw.dt", function() {
    $(".spinner-border").css("display", "none");//Ẩn spinner đi
  });

  // Lắng nghe sự kiện click icon edit(truy vấn ủy quyền)
  $("#orders-table").on("click", ".update-icon", function () {
    onIconUpdateOrderClick(this);
  });

  // Lắng nghe sự kiện click icon delete(truy vấn ủy quyền)
  $("#orders-table").on("click", ".delete-icon", function () {
    onIconDeleteOrderClick(this);
  });

  // Lắng nghe sự kiện click nút cập nhật trên modal update
  $("#btn-update-confirm").on("click", onBtnUpdateConfirmClick);

  // Lắng nghe sự kiện click nút xác nhận trên modal delete
  $("#btn-delete-confirm").on("click", function () {
    callAPIDeleteOrder();
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm xử lý tải trang
  function onPageLoading() {
    "use strict";
    // gọi hàm xử lý call API get orders list
    callAPIGetOrdersList();
  }

  // Hàm xử lý click icon update
  function onIconUpdateOrderClick(paramElement) {
    "use strict";
    //B1: Thu thập dữ liệu
    let vOrderObj = collectDataGetOrderToUpdateForm(paramElement);
    //B2: Kiểm tra dữ liệu(bỏ qua)
    //B3: Call API và xử lý hiển thị
    handleDataGetOrderToUpdateForm(vOrderObj);
  }

  // Hàm xử lý click icon delete
  function onIconDeleteOrderClick(paramElement) {
    "use strict";
    $("#modal-delete-order").modal(); //hiển thị modal
    //lấy dữ liệu id cần xóa
    let vRowClick = $(paramElement).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id để tiến hành update
  }

  // Hàm xử lý ấn nút confirm update
  function onBtnUpdateConfirmClick() {
    "use strict";
    //B0: Khai báo đối tượng
    let vUpdatedOrder = {
      firstName: "",
      lastName: "",
      email: "",
      address: "",
      phone: "",
      methodPayment: "",
      voucherId: "",
    };
    //B1: Thu thập dữ liệu
    collectDataUpdateOrder(vUpdatedOrder);
    //B2: Kiểm tra dữ liệu
    let vCheck = validateDataUpdateOrder(vUpdatedOrder);
    if (vCheck) {
      //B3: Call API update order
      callAPIUpdateOrder(vUpdatedOrder);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm xử lý call API get orders list
  function callAPIGetOrdersList() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/orders",
      type: "GET",
      dataType: "json",
      success: function (paramResponse) {
        getOrdersDataToTable(paramResponse); //gọi hàm load danh sách đơn hàng vào table
      },
      error: function (paramError) {
        alert("Có lỗi xảy ra, vui lòng tải lại trang");
        console.log(paramError.responseText);
      },
    });
  }

  // Hàm xử lý call API get all vouchers
  function callAPIGetVouchersList() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/vouchers",
      type: "GET",
      dataType: "json",
      success: function (paramResponse) {
        gVoucherListArr = paramResponse; //gán giá trị vouchers list vào biến toàn cục
      },
      error: function (paramError) {
        alert("Có lỗi xảy ra, vui lòng thử lại");
        console.log(paramError.responseText);
      },
    });
  }

  // Hàm xử lý call API check voucher
  function callAPICheckVoucher(paramVoucher) {
    "use strict";
    let vSearchParams = new URLSearchParams(paramVoucher);
    console.log(gBASE_URL + "/vouchers" + "?" + vSearchParams.toString());
    $.ajax({
      url: gBASE_URL + "/vouchers" + "?" + vSearchParams.toString(),
      type: "GET",
      dataType: "json",
      async: false,//phải trả về ngay vì còn sử dụng để update
      success: displayDataCheckVoucherSuccess,
      error: function (paramError) {
        alert("Không tìm thấy voucher, vui lòng thử lại");
        console.log(paramError.responseText);
      },
    });
  }

  // Hàm call API update order
  function callAPIUpdateOrder(paramOrderObj) {
    "use strict";
    console.log(gBASE_URL + "/orders/" + gId);
    $.ajax({
      url: gBASE_URL + "/orders/" + gId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramOrderObj),
      dataType: "json",
      success: displayDataUpdateOrderSuccess,
      error: function (paramError) {
        alert("Cập nhật đơn hàng thất bại, vui lòng thử lại");
        console.assert(paramError.responseText);
      },
    });
  }

  // Hàm call API xóa order
  function callAPIDeleteOrder() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/orders/" + gId,
      type: "DELETE",
      dataType: "json",
      success: function (paramResponse) {
        alert("Xóa đơn hàng thành công");
        $("#modal-delete-order").modal("hide");
        onPageLoading();
      },
      error: function (paramError) {
        alert("Xóa đơn hàng thất bại, vui lòng thử lại");
        console.log(paramError.responseText);
      },
    });
  }

  // Hàm xử lý load orders data vào table(gọi là data vì nếu có chức năng lọc thì sẽ ko hiển thị cả list)
  function getOrdersDataToTable(paramOrderObj) {
    "use strict";
    gSTT = 1; //reset STT
    gTable.clear(); //xóa trắng bảng
    gTable.rows.add(paramOrderObj); //add thêm dữ liệu vào các rows
    gTable.draw(); //vẽ lại bảng
  }

  // Hàm xử lý thu thập dữ liệu get order by Id
  function collectDataGetOrderToUpdateForm(paramElement) {
    "use strict";
    let vRowClick = $(paramElement).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id để tiến hành update
    return vRowData;
  }

  // Hàm xử lý đổ dữ liệu vào form update
  function handleDataGetOrderToUpdateForm(paramOrderObj) {
    "use strict";
    // Đổ dữ liệu vào form
    $("#input-update-lastname").val(paramOrderObj.lastName);
    $("#input-update-firstname").val(paramOrderObj.firstName);
    $("#input-update-email").val(paramOrderObj.email);
    $("#input-update-phone").val(paramOrderObj.phone);
    $("#input-update-address").val(paramOrderObj.address);
    $("#select-update-payment-methor").val(paramOrderObj.methodPayment);
    // // Sử dụng vòng lặp while để tìm trong mảng voucher đã lưu ở global voucher theo voucherId
    if (paramOrderObj.voucherId) {//Kiểm tra xem order có tồn tại voucherId ko
      let vFound = false;
      let vIndex = 0;

      while (!vFound && vIndex < gVoucherListArr.length) {
        if (paramOrderObj.voucherId == gVoucherListArr[vIndex].id) {
          $("#input-update-voucher").val(gVoucherListArr[vIndex].voucherCode);
          $("#input-update-discount").html(gVoucherListArr[vIndex].discount);
          vFound = true;
        } else {
          vIndex++;
        }
      }

      // Nếu không tìm thấy voucher, gán giá trị mặc định
      if (!vFound) {
        $("#input-update-voucher").val("");
        $("#input-update-discount").html(0);
      }
    } else {
      // Nếu không có voucherId, cũng gán giá trị mặc định
      $("#input-update-voucher").val("");
      $("#input-update-discount").html(0);
    }

    //Hiển thị modal
    $("#modal-update-order").modal();
  }

  // Hàm xử lý thu thập dữ liệu update order
  function collectDataUpdateOrder(paramOrderObj) {
    "use strict";
    paramOrderObj.lastName = $("#input-update-lastname").val();
    paramOrderObj.firstName = $("#input-update-firstname").val();
    paramOrderObj.email = $("#input-update-email").val();
    paramOrderObj.phone = $("#input-update-phone").val();
    paramOrderObj.address = $("#input-update-address").val();
    paramOrderObj.methodPayment = $("#select-update-payment-methor").val();
    // Lấy voucherCode và gọi API checkVoucher
    let vQueryVoucherCode = {
      voucherCode: "",
    };
    vQueryVoucherCode.voucherCode = $("#input-update-voucher").val();
    //Có nhập voucher và đúng định dạng
    if (
      !isNaN(vQueryVoucherCode.voucherCode) &&
      vQueryVoucherCode.voucherCode != ""
    ) {
      //API này nếu vocherId rỗng sẽ trả về cả mảng voucher => lỗi
      //callAPI check voucher
      callAPICheckVoucher(vQueryVoucherCode);
    }
    //Nhập ko đúng định dạng
    if (isNaN(vQueryVoucherCode.voucherCode)) {
      gVoucherId = null; //gán bằng null
    }
    //nếu ko nhập voucher thì sẽ ko thực hiện call API,
    if (vQueryVoucherCode.voucherCode == "") {
      gVoucherId = -1; //Gán gVoucherId = "" để phân biệt với trường hợp voucher sai
    }
    //Gán giá trị voucherId của đối tượng
    paramOrderObj.voucherId = gVoucherId;
    console.log(gVoucherId);
  }

  // Hàm xử lý hiển thị data check voucher thành công
  function displayDataCheckVoucherSuccess(paramVoucherResponse) {
    "use strict";
    if (paramVoucherResponse.length > 0) {
      //Lưu lại giá trị voucherId để lát update
      gVoucherId = paramVoucherResponse[0].id;
    } else {
      //call API với voucher sai vẫn trả về nhưng là mảng rỗng
      gVoucherId = null; //gán thành null để báo voucher ko hợp lệ
    }
  }

  // Hàm xử lý kiểm tra dữ liệu update order
  function validateDataUpdateOrder(paramOrderObj) {
    "use strict";
    if (paramOrderObj.lastName == "") {
      alert("Last Name không được để trống");
      return false;
    }
    if (paramOrderObj.firstName == "") {
      alert("First Name không được để trống");
      return false;
    }
    if (paramOrderObj.email == "") {
      alert("Email không được để trống");
      return false;
    }
    let vEmailCheck = validateEmail(paramOrderObj.email);
    if (!vEmailCheck) {
      alert("Email không đúng định dạng");
      return false;
    }
    if (paramOrderObj.phone == "") {
      alert("Số điện thoại không được để trống");
      return false;
    }
    let vNumberPhoneCheck = validateNumberPhone(paramOrderObj.phone);
    if (!vNumberPhoneCheck) {
      alert("Số điện thoại không đúng định dạng");
      return false;
    }
    if (paramOrderObj.address == "") {
      alert("Địa chỉ không được để trống");
      return false;
    }
    if (paramOrderObj.methodPayment == "") {
      alert("Vui lòng chọn phương thức thanh toán");
      return false;
    }
    if (paramOrderObj.voucherId == null) {
      alert("Voucher không hợp lệ");
      return false;
    }
    return true;
  }

  // Hàm xử lý hiển thị update order thành công
  function displayDataUpdateOrderSuccess(paramResponse) {
    "use strict";
    alert("Cập nhật đơn hàng thành công");
    gVoucherId = ""; //Cập nhật lại voucherId, tránh bị lẫn với các đơn ko có voucher
    //Xóa thông tin trên form
    $("#input-update-lastname").val("");
    $("#input-update-firstname").val("");
    $("#input-update-email").val("");
    $("#input-update-phone").val("");
    $("#input-update-address").val("");
    $("#select-update-payment-methor").val();
    $("#input-update-voucher").val("Paypal");

    $("#modal-update-order").modal("hide"); //ẩn modal success
    //load lại trang
    onPageLoading();
  }

  /*hàm kiểm tra email
      Nếu khớp, test() trả về true; nếu không khớp, nó trả về false.
      */
  function validateEmail(paramEmail) {
    "use strict";
    let vFormatReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let vCheck = vFormatReg.test(paramEmail);
    return vCheck;
  }

  /*hàm kiểm tra sđt
          Nếu khớp, test() trả về true; nếu không khớp, nó trả về false.
          */
  function validateNumberPhone(paramNumberPhone) {
    "use strict";
    let vFormatReg = /^(?:\+84|0)[0-9]{8,9}$/;
    let vCheck = vFormatReg.test(paramNumberPhone);
    return vCheck;
  }
});
